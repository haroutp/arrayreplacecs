﻿using System;

namespace ArrayReplace
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
        int[] arrayReplace(int[] inputArray, int elemToReplace, int substitutionElem) {
            int[] arrToReturn = new int[inputArray.Length];
            
            for(int i = 0; i < inputArray.Length; i++){
                if(inputArray[i] == elemToReplace){
                    arrToReturn[i] = substitutionElem;
                } else {
                    arrToReturn[i] = inputArray[i];
                }
                
            }
            
            return arrToReturn;
        }

    }
}
